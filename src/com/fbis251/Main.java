package com.fbis251;

import com.fbis251.model.ApiClient;
import com.fbis251.model.BladeAchievement;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        ApiClient apiClient = new ApiClient();

        List<BladeAchievement> bladeAchievementList = apiClient.getAchievementsFromSteamApi();

        for (BladeAchievement bladeAchievement : bladeAchievementList) {
            System.out.println("Main.main(): bladeAchievement = [" + bladeAchievement + "]");
        }
    }
}
