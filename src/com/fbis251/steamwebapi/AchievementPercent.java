package com.fbis251.steamwebapi;

public class AchievementPercent {
    private String id;
    private double percent;

    public AchievementPercent(String id, double percent) {
        this.id = id;
        this.percent = percent;
    }

    public String getId() {
        return id;
    }

    public double getPercent() {
        return percent;
    }
}
