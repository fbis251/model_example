package com.fbis251.steamwebapi;

import java.util.ArrayList;
import java.util.List;

public class SteamWebApi {

    public List<AchievementInfo> getAchievementInfo() {
        List<AchievementInfo> result = new ArrayList<>();

        result.add(new AchievementInfo("one", "Name One", "Description One", "https://example.com/one.png"));
        result.add(new AchievementInfo("two", "Name Two", "Description Two", "https://example.com/two.png"));
        result.add(new AchievementInfo("three", "Name Three", "Description Three", "https://example.com/three.png"));

        return result;
    }

    public List<AchievementPercent> getAchievementPercent() {
        List<AchievementPercent> result = new ArrayList<>();

        result.add(new AchievementPercent("one", 50.3));
        result.add(new AchievementPercent("two", 16.56));
        result.add(new AchievementPercent("three", 33.14));

        return result;
    }
}
