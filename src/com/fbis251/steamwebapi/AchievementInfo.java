package com.fbis251.steamwebapi;

public class AchievementInfo {
    private String id;
    private String name;
    private String description;
    private String imageUrl;

    public AchievementInfo(String id, String name, String description, String imageUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }
}
