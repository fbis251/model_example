package com.fbis251.model;

public class BladeAchievement {
    private String name;
    private String description;
    private String imageUrl;
    private double percent;

    public BladeAchievement(String name, String description, String imageUrl, double percent) {
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
        this.percent = percent;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BladeAchievement{");
        sb.append("name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", imageUrl='").append(imageUrl).append('\'');
        sb.append(", percent=").append(percent);
        sb.append('}');
        return sb.toString();
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public double getPercent() {
        return percent;
    }
}
