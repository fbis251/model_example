package com.fbis251.model;

import com.fbis251.steamwebapi.AchievementInfo;
import com.fbis251.steamwebapi.AchievementPercent;
import com.fbis251.steamwebapi.SteamWebApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApiClient {
    /**
     * Performs two steam web API calls to get the achievement information (name, description, image URL) and the
     * percentage of players who have attained the achievement (percent)
     *
     * @return A List of model objects that will be returned to a browser as a JSON response.
     */
    public List<BladeAchievement> getAchievementsFromSteamApi() {
        SteamWebApi steamWebApi = new SteamWebApi();

        List<AchievementInfo> achievementInfos = steamWebApi.getAchievementInfo();
        List<AchievementPercent> achievementPercents = steamWebApi.getAchievementPercent();

        Map<String, Double> percentMap = new HashMap<>();

        for (AchievementPercent achievementPercent : achievementPercents) {
            percentMap.put(achievementPercent.getId(), achievementPercent.getPercent());
        }

        List<BladeAchievement> bladeAchievementsList = new ArrayList<>();
        for (AchievementInfo achievementInfo : achievementInfos) {
            if (!percentMap.containsKey(achievementInfo.getId())) {
                // Skip any achievements that don't have a corresponding percent
                continue;
            }
            // The achievement percent was contained in our map, pull the value before we use it in the model class
            double percent = percentMap.get(achievementInfo.getId());

            // If we got this far, there was a percent available for this achievement. We need to create a new model for
            // our JSON response and add it to our bladeAchievementsList before returning the bladeAchievementsList
            // to the client.

            BladeAchievement newBladeAchievement = new BladeAchievement(achievementInfo.getName(),
                    achievementInfo.getDescription(),
                    achievementInfo.getImageUrl(),
                    percent);

            // After we instantiate the newBladeAchievement, we can add it to the List that is going to be returned
            // to the client
            bladeAchievementsList.add(newBladeAchievement);
        }

        return bladeAchievementsList;
    }
}
